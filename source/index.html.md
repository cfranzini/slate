---
title: EPONA Docs

language_tabs: # must be one of https://git.io/vQNgJ
  - shell
#  - ruby
#  - json
#  - sql
#  - python
#  - javascript

toc_footers:
  - <a href='https://bitbucket.org/st-elias/epona-app' target="_blank">View Source Code</a>
  - <a href='https://github.com/lord/slate' target="_blank">Documentation Powered by Slate</a>

includes:
#  - errors

search: true
---

# EPONA Documentation

Welcome to the EPONA documentation. EPONA is an operational management and business intelligence web application specificially tailored to the thoroughbred horse-racing industry. You can visit the live production site at <a href="https://epona.steliasstables.com" target="_blank">epona.steliasstables.com</a>.

The application is built on the <a href="https://activeadmin.info/index.html" target="_blank">Active Admin</a> framework for <a href="http://rubyonrails.org/" target="_blank">Ruby on Rails</a> and an <a href="http://aws.amazon.com" target="_blank">Amazon Web Services (AWS)</a> architecture.

### Where does the name EPONA come from?

<a href="https://en.wikipedia.org/wiki/Epona" target="_blank">Epona</a> is the goddess of horses in the Gallo-Roman religion. It is also the name of <a href="https://en.wikipedia.org/wiki/Epona_(The_Legend_of_Zelda)" target="_blank">Link's horse</a> in *The Legend of Zelda: Ocarina of Time*.

# The EPONA-verse

## Source Code

All EPONA source code and scripts are hosted on Bitbucket. You can access the project folder and repositories at <a href="https://bitbucket.org/account/user/st-elias/projects/EPONA" target="_blank">bitbucket.org/account/user/st-elias/projects/EPONA</a>.

### epona-app

The Rails app source code is located in the <a href="https://bitbucket.org/st-elias/epona-app" target="_blank">epona-app</a> repository.

### Other Repositories

The other repositories in the EPONA project folder include a collection of data scrapers and analysis scripts.

## Architecture

![EPONA Architecture](https://s3.amazonaws.com/epona-docs/imgs/epona-aws-architecture+(1).png "EPONA Architecture")

The web application runs on an AWS cloud architecture operated by a dedicated account. To access the account visit <a href="https://st-elias.signin.aws.amazon.com/console" target="_blank">st-elias.signin.aws.amazon.com/console</a>.

To request user access for the account contact cfranzini@tbfollc.com.

### Account Access

**Identity Access Management (IAM)**

The following users currently exist.

| User Name     | Groups         | MFA      |
|---------------|----------------|----------|
| root          | super user     | enabled  |
| chrisfranzini | admin, finance | enabled  |
| davidhedley   | admin          | disabled |
| davidschwetz  | admin          | disabled |

#### Users

The _root_ user access keys have been destroyed. Tasks requiring root-level permissions can be performed by logging in to the AWS Console using the root user credentials. **Root user credentials should only be used when absolutely necessary and should not be used for day-to-day operations**. For administrative tasks use the current administrator account or assign a user to the _admin_ group.

The _chrisfranzini_ user is the current administrator for the account.

The _davidschwetz_ user was the prior administrator for the account and performed the initial setup for the account.

The _davidhedley_ user was created to assist with migrating the production database to RDS.

#### Groups

* **admin**: AWS-managed AdministratorAccess policy
* **finance**: user-managed BillingFullAccess policy

#### Policies

* **AdministratorAccess**: grants administrator permissions (_json_)

> AdministratorAccess policy:

```shell
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "*",
      "Resource": "*"
    }
  ]
}
```
* **BillingFullAccess**: grants access to account billing dashboard (_json_)

> BillingFullAccess policy:

```shell
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "aws-portal:*",
            "Resource": "*"
        }
    ]
}
```

**VPC**

**Security Groups**


### Servers (EC2)

#### Instances

#### EBS Volumes

#### Authentication

### Database (RDS)

#### Backups



### Storage (S3)

### Hosting

#### Elastic IPs

#### Route 53

### Management

#### CloudWatch

#### Billing

## Data

### Sources

#### Sales

#### Racing

#### Farms and Trainers

#### Finances

### Backups

# Getting Started

## Dependencies

EPONA depends on the following programs and packages. This guide assumes the local development machine is running macOS and you have Admin and <a href="https://en.wikipedia.org/wiki/Superuser" target="_blank">SuperUser (su)/Root</a> privileges.

<a href="https://brew.sh/" target="_blank">Homebrew</a> is recommended for all package installations.

**Tip:** Install and/or update <a href="https://developer.apple.com/xcode/" target="_blank">Xcode</a> and agree to the developer license beforehand to save time and hassle.

### git

<a href="https://git-scm.com/" target="_blank">Git</a> is a version control system for managing source code.

* <a href="https://www.atlassian.com/git/tutorials/install-git" target="_blank">Install Git</a> (Homebrew: <a href="http://formulae.brew.sh/formula/git" target="_blank">git</a>)

### ruby

The web application is written entirely in <a href="https://www.ruby-lang.org/en/" target="_blank">Ruby</a>. It makes use of <a href="http://rubyonrails.org/" target="_blank">Ruby on Rails</a>, a Ruby-specific web-application framework.

<a href="https://rvm.io/" target="_blank">Ruby Version Manager (rvm)</a> is recommended for installing Ruby and Rails.

* rvm requires <a href="https://gnupg.org/" target="_blank">GnuPG (GPG)</a>, an open encryption standard and key management system (Homebrew: <a href="http://formulae.brew.sh/formula/gnupg" target="_blank">gnupg</a>)  
* <a href="https://rvm.io/rvm/install" target="_blank">Install rvm</a>

**Tip:** Use `\curl -sSL https://get.rvm.io | bash -s stable --rails` to install rvm, Ruby, and Rails all at once.

> postgresql:

```shell
# initialize PostgreSQL database
localhost:~$ initdb -D /usr/local/var/postgres

# start PostgreSQL server
localhost:~$ pg_ctl -D /usr/local/var/postgres -l logfile start

# create postgres superuser
localhost:~$ psql postgres
postgres=# CREATE ROLE pgtestuser SUPERUSER LOGIN;
postgres=# ALTER USER pgtestuser WITH PASSWORD 'pgtestuser';
```

### postgresql

<a href="https://www.postgresql.org/" target="_blank">PostgreSQL</a> is an open-source object-relational database system built for the <a href="https://en.wikipedia.org/wiki/SQL" target="_blank">Structured Query Language (SQL)</a>.

* Install PostgreSQL (Homebrew: <a href="http://formulae.brew.sh/formula/postgresql" target="_blank">postgresql</a>)
* Initialize PostgreSQL server on local machine (<a href="https://chartio.com/resources/tutorials/how-to-start-postgresql-server-on-mac-os-x/" target="_blank">How to Start PostgreSQL Server on Mac OS X</a>) (>>)
* Log on to the `postgres` database and create a superuser if one does not exist (>>)


### ssh keys

<a href="https://en.wikipedia.org/wiki/Secure_Shell" target="_blank">Secure Shell (SSH)</a> <a href="https://wiki.archlinux.org/index.php/SSH_keys" target="_blank">keys</a> are a form of authentication for secure remote access to servers.

EPONA development requires two sets of keys: a personal key-pair for Bitbucket and a shared key-pair for accessing the application servers.

* Bitbucket:
  * Create a personal SSH key (<a href="https://confluence.atlassian.com/bitbucketserver/creating-ssh-keys-776639788.html" target="_blank">Creating SSH Keys</a>)  
  * Link the SSH key to the St. Elias Bitbucket account (<a href="https://confluence.atlassian.com/bitbucketserver/ssh-user-keys-for-personal-use-776639793.html" target="_blank">Adding an SSH key to your Bitbucket account</a>)


* Servers:  
  * Copy `horse-keys.pem` to the `~/.ssh` directory

## Local Development Setup

1) Clone a copy of the <a href="https://bitbucket.org/st-elias/epona-app" target="_blank">epona-app</a> repository on Bitbucket to the local machine

  `localhost:~$ git clone git@bitbucket.org:st-elias/epona-app.git`

2) Set Ruby version to 2.3.1 in `~/epona-app`

`localhost:~/epona-app$ rvm install 2.3.1`  
`localhost:~/epona-app$ rvm use 2.3.1`

> Create a copy of the production database:

```shell
# SSH on to horse_toc
localhost:~$ ssh -i ~/.ssh/horse-keys.pem ubuntu@ec2-34-224-233-143.compute-1.amazonaws.com

# Dump and zip production database
ubuntu@ip-172-31-20-94:~$ pg_dump -h horsedb.ci44sgikzbpn.us-east-1.rds.amazonaws.com -U horse_live -f horse_live_dump.sql horse_live
ubuntu@ip-172-31-20-94:~$ gzip horse_live_dump.sql

# SCP database dump to local machine and unzip:
localhost:~$ scp -i ~/.ssh/horse-keys.pem ubuntu@ec2-34-224-233-143.compute-1.amazonaws.com:horse_live_dump.sql.gz ./
localhost:~$ gzip -d horse_live_dump.sql.gz
```

3) Create a copy of the production database (>>)

* SSH on to `horse_toc` to enter the VPC

  * <a href="https://www.digitalocean.com/community/tutorials/how-to-configure-custom-connection-options-for-your-ssh-client" target="_blank">Defining SSH Hosts</a> (optional)

* Dump the contents of the production database into a `.sql` file on `horse_toc`

* <a href="https://ss64.com/bash/scp.html" target="_blank">Secure-copy (SCP)</a> database dump to local machine and unzip

> Set up local development database:

```shell
# create development database
localhost:~/epona-app$ rake db:create

# grant necessary roles
localhost:~/epona-app$ psql -U horse_live
horse_live=# CREATE EXTENSION pg_stat_statements;
horse_live=# GRANT CONNECT ON DATABASE horse_live TO blazer_reader;
horse_live=# GRANT ALL PRIVILEGES ON DATABASE horse_live TO horse_live;

# load database
localhost:~/epona-app$ pg_dump -f ~/horse_live_dump.sql horse_live -U horse_live

# run migrations
localhost:~/epona-app$ rake db:migrate
```

4) Set up local development database (>>)

**Tip:** To restore the development database follow these steps but be sure to drop the local horse_live database beforehand.




* Create the development database
  * **All <a href="https://ruby.github.io/rake/" target="_blank">Rake</a> tasks must be run from the `epona-app` directory**

* Ensure database roles have appropriate privileges

* Load database with production dump file

* Run migration task to ensure all migrations are executed

5) Test setup

* Run `rails console` (`rails c`) to connect to the development database via the <a href="http://guides.rubyonrails.org/command_line.html" target="_blank">Rails command line</a>
* Run `rails server` (`rails s`) and visit `localhost:3000` in a browser to see the development application


## Deploying to Production

Production deployment is handled by <a href="http://capistranorb.com/" target="_blank">Capistrano</a>, a deployment tool for Ruby.

Capistrano deploys the current version of the `epona-app` `master` branch on Bitbucket. **Despite the fact that deployment is executed locally, Capistrano does not care about the state of any local branches**. Additionally, the production server ignores a handful of important configuration files in favor of local versions of the files. For this reason, a number of configuration files **should differ** between your local machine and the production server in order to deploy the application properly. Even though the Bitbucket repository is private, it is good practice to **push development configurations rather than production configurations to Bitbucket**, and **all secrets should be hidden as environment variables**. The necessary production configuration files can be edited directly on the production server using a command line editor, such as <a href="https://vim.sourceforge.io/" target="_blank">Vim</a>.







> Deploy to production:  

```shell
~/epona-app $ cap production deploy -t
```
Deployment configurations for Capistrano are located in config/deploy.rb.
